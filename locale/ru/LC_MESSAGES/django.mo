��    &      L  5   |      P  )   Q  	   {     �     �     �  
   �     �     �     �  9   �     
               &  	   ,     6     ;  	   X     b     p     y  F   �     �     �     �  !   �            1   '  (   Y     �  "   �     �     �     �     �     �  �  �  P   �          $     A  #   H     l     �  
   �     �  u   �     ;	     U	  
   b	     m	     v	     �	  2   �	     �	     �	     �	      
  �   
  $   �
     �
  (   �
  H   
     S  &   Z  f   �  E   �  7   .  V   f  
   �  )   �     �          +                                      !   $                                            "                                #       	                 &             
                    %    A user with that username already exists. Add place Address Alas All rights reserved. Contact us Create event Date Description Designates whether the user can log into this admin site. Enter place Latitude Log in Login Longitude Name Nefar - just look for events Organizer Participators Password Phone number Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. Sign up Signup Telegram username This feature is under development Type Update my browser Update your browser to display the site correctly We could not find anything in this place Your browser is not supported Your place will be considered soon email staff status user username users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Пользователь с таким именем уже существует. Добавить место Местоположение Увы Все права защищены. Связаться с нами Создать событие Время Описание Определяет, может ли пользователь войти на сайт администратора. Введите место Широта Войти Вход Долгота Название Nefar — события искать просто Организатор Участники Пароль Номер телефона Обязательное поле. 150 символов или меньше. Только буквы, цифры и знаки @/./+/-/_ Зарегистрироваться Регистрация Имя пользователя Telegram Этот функционал находится в разработке Тип Обновить мой браузер Обновите ваш браузер для правильного отображения сайта Мы не смогли найти ничего в этом месте Ваш браузер не поддерживается Ваше место будет рассмотрено в ближайшее время почта статус администратора пользователь имя пользователя пользователи 