const R1 = gettext("Add existing place");
const R2 = gettext("Details");
const R3 = gettext("All");

function strToBounds(str) {
    return new google.maps.LatLngBounds().union(str);
}

function dictToUrlParams(dict) {
    return Object.keys(dict).length === 0 ? '' :
        '?' + Object.keys(dict).map(key =>
        key + '=' + encodeURIComponent(JSON.stringify(dict[key]))
        ).join('&');
}

function getUrlParams() {
    let paramsSearcher = new URLSearchParams(window.location.search);

    let params = {};
    for (let param of paramsSearcher.keys()) {
        params[param] = JSON.parse(paramsSearcher.get(param));
    }

    if (!('stage' in params)) {
        params.stage = 0;
    }
    return params;
}

function getCssVar(property) {
    return getComputedStyle(document.documentElement).getPropertyValue('--' + property);
}

// Возвращает res при выполнении условия cond
function condReturn(res, cond) {
    return new Promise(resolve => {
        let checkExist = setInterval(() => {
            if (cond()) {
                resolve(res());
                clearInterval(checkExist);
            }
        }, 100);
    });
}

// Возвращает y-координату низа HTML элемента
function getElemBottom(elem) {
    let elemRect = elem.getBoundingClientRect();
    return elemRect.top - document.body.getBoundingClientRect().top + elemRect.height;
}

function latLngToPoint(latLng, map) {
    const topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
    const bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
    const scale = Math.pow(2, map.getZoom());
    const worldPoint = map.getProjection().fromLatLngToPoint(latLng);
    return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
}

function pointToLatLng(point, map) {
    const topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
    const bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
    const scale = Math.pow(2, map.getZoom());
    const worldPoint = new google.maps.Point(point.x / scale + bottomLeft.x, point.y / scale + topRight.y);
    return map.getProjection().fromPointToLatLng(worldPoint);
}

function ajaxPost(input, params, body) {
    return fetch('/' + input + '/' + dictToUrlParams(params),
        {method: 'POST', headers: {'X-CSRFToken': Cookies.get('csrftoken')}, body: body});
}

async function ajaxGetJSON(input, params) {
    return (await fetch('/' + input + '/' + dictToUrlParams(params), {method: 'GET'})).json();
}

function isMobile() {
    return window.matchMedia(`(max-width: ${getCssVar('mobile-width')}`).matches;
}

// function getElementsByXPath(expression) {
//     let results = [];
//     let query = document.evaluate(expression, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
//     for (let i = 0, length = query.snapshotLength; i < length; i++)
//         results.push(query.snapshotItem(i));
//     return results;
// }

const NefarApp = new function () {
    class Window {
        constructor(elemId, doneCallback) {
            this.pageElem = document.getElementById(elemId);
            this.doneCallback = doneCallback;
        }

        async show() {
            this.pageElem.style.display = '';
        }

        hide() {
            this.pageElem.style.display = 'none';
        }
    }

    class Modal {
        constructor(modalClass, onShow = () => {
        }, onDone = () => {
        }, onHide = () => {
        }) {
            this.modalClass = modalClass;
            this.onShow = onShow;
            this.onDone = onDone;
            this.onHide = onHide;

            this.pageElem = document.querySelector('#modal');

            this.pageElem.querySelector('#modal__close').onclick = () => this.hide();

            this.pageElem.querySelector('#modal__done-button').addEventListener('click', () => {
                this.onDone();
                this.hide();
            });

            window.onclick = event => {
                if (event.target === this.pageElem) {
                    this.hide();
                }
            };
        }

        hide() {
            this.onHide();
            this.pageElem.style.display = 'none';
            this.pageElem.querySelector('#modal__body_' + this.modalClass).style.display = 'none';
            this.pageElem.querySelector('#modal__header-text_' + this.modalClass).style.display = 'none';
        }

        show() {
            this.onShow();
            this.pageElem.style.display = '';
            this.pageElem.querySelector('#modal__body_' + this.modalClass).style.display = '';
            this.pageElem.querySelector('#modal__header-text_' + this.modalClass).style.display = '';
        }
    }

    class Card {
        constructor(templateId) {
            let templateElem = NefarApp.MapWindow.pageElem.querySelector('#' + templateId + '-template');
            this.parentElem = templateElem.parentElement;
            this.parentElem.appendChild(templateElem.content.cloneNode(true));
            this.pageElem = this.parentElem.querySelector('#' + templateId);
            this.isShown = true;
        }

        close() {
            this.isShown = false;
        }
    }

    /**
     * @param {Card} Card
     */
    const Mobile = Card => class extends Card {
        constructor(templateId, params) {
            super(templateId, params);
            this.parentElem.querySelector('#map-window__map').style.display = 'none';
            this.pageElem.querySelector('#topic__close-button').style.display = '';
            this.pageElem.querySelector('#topic__close-button').addEventListener('click', () => {
                this.close();
                // noinspection JSIgnoredPromiseFromCall
                NefarApp.changeStage(Object.assign(NefarApp.params, {stage: 2}));
            });
        }

        close() {
            super.close();
            this.parentElem.querySelector('#map-window__map').style.display = '';
            this.pageElem.remove();
        }
    };

    /**
     * @param {Card} Card
     */
    const Desktop = Card => class extends Card {
        constructor(templateId) {
            super(templateId);

            let marker = NefarApp.MapWindow.indexMarkerMap.get(NefarApp.params.markerIndex);
            let map = NefarApp.MapWindow.map;
            let delta = 30;
            this.infoWindow = new google.maps.InfoWindow({content: this.pageElem, maxWidth: 640, disableAutoPan: true});
            this.infoWindow.addListener('closeclick', () => {
                this.close();
                // noinspection JSIgnoredPromiseFromCall
                NefarApp.changeStage(Object.assign(NefarApp.params, {stage: 2}));
            });
            let oneTimeListener = google.maps.event.addListener(this.infoWindow, 'domready', async () => {
                google.maps.event.removeListener(oneTimeListener);

                const wait = () => {
                    if (!this.pageElem.parentElement) {
                        return;
                    }
                    if (!this.pageElem.parentElement.style.maxHeight) {
                        console.log((new Date).getTime()); // Если повторяется больше одного раза увеличить
                        return setTimeout(wait, 20);
                    }

                    this.pageElem.style.height = (parseInt(this.pageElem.parentElement.style.maxHeight) - delta) + 'px';

                    let tempPoint = latLngToPoint(marker.getPosition(), map);
                    tempPoint.y -=
                        ((parseInt(this.pageElem.parentElement.style.maxHeight) + 56) / 2.0)
                        - delta;

                    if (Object.values(tempPoint).findIndex(Number.isNaN) === -1) {
                        map.panTo(pointToLatLng(tempPoint, map));
                    }
                };
                wait();
            });

            this.infoWindow.open(map, marker);
        }

        close() {
            super.close();
            this.infoWindow.close();
        }
    };

    /**
     * @param {Desktop, Mobile} Platform
     */
    const EventPlace = Platform => class extends Platform {
        constructor(placeDetails) {
            super('card_event-place');

            this.placeName = placeDetails.name;
            this.pageElem.querySelector('.card__topic .card__header').innerText = this.placeName;
            this.pageElem.querySelector('#card__image').src = '/' + placeDetails.image;
            this.pageElem.querySelector('#card__description').innerText = placeDetails.description;

            for (let event of placeDetails.events) {
                let row = this.pageElem.querySelector('#card__schedule tbody').insertRow(0);
                row.innerHTML = `
                    <td><i class="${event['type__icon']}"></i></td>
                    <td>${event['name']}</td>
                    <td>${event['time']}</td>
                `;
                let detailsButtonCell = row.insertCell(3);
                detailsButtonCell.innerHTML = R2;
                detailsButtonCell.classList.add('details-button');
                detailsButtonCell.addEventListener('click',
                    () => NefarApp.changeStage(
                        Object.assign(NefarApp.params, {stage: 4, placeName: this.placeName, eventId: event.id})));
            }

            this.pageElem.querySelector('#create-event-button').addEventListener('click', () => {
                new Modal('not-done').show();
            });
        }
    };

    /**
     * @param {Desktop, Mobile} Platform
     */
    const UserBars = Platform => class extends Platform {
        constructor(eventDetails) {
            super('card_user-bars', NefarApp.MapWindow.indexMarkerMap.get(NefarApp.params.markerIndex));

            this.pageElem.querySelector('.card__topic .card__header').innerText = NefarApp.params.placeName;
            this.pageElem.querySelector('#topic__back-button').addEventListener('click', () => {
                // noinspection JSIgnoredPromiseFromCall
                NefarApp.changeStage(Object.assign(NefarApp.params, {stage: 3}));
            });

            this.fillCardSection('organizer', [eventDetails['organizer']], (name) => {
                window.open('https://t.me/' + name, '_blank').focus();
            });

            this.fillCardSection('participators', eventDetails['users'], (name) => {
                window.open('https://t.me/' + name, '_blank').focus();
            });
        }

        fillCardSection(sectionId, userNames, barClickCallback) {
            for (let name of userNames) {
                let section = this.pageElem.querySelector('#card__section_' + sectionId);
                let barDocFragment = this.parentElem.querySelector('#user-bar-template').content.cloneNode(true);
                barDocFragment.querySelector('.user-bar__name').innerText = '@' + name;
                section.appendChild(barDocFragment);
                section.querySelector('.user-bar:last-child').addEventListener('click', () => barClickCallback(name));
            }
        }
    };

    class EventTypeCell {
        constructor(icon, name, callback) {
            this.docFragment = document.querySelector('#event-types-window__cell-template').content.cloneNode(true);
            this.docFragment.querySelector('i').className += ' ' + icon;
            this.docFragment.querySelector('.event-type-cell__name').innerHTML += name;
            this.callback = callback;
        }

        get pageElem() {
            let elem = this.docFragment.cloneNode(true);
            elem.querySelector('.event-type-cell__hover-bg').addEventListener('click', this.callback);
            return elem;
        }
    }

    this.MapWindow = new class extends Window {
        constructor() {
            super('map-window');
            this.geocoder = new google.maps.Geocoder;
            this.newPlaceMarker = null;
            this.indexMarkerMap = new Map();
            this.map = null;
            this.canPlaceMarker = false;
            this.addMarkerButton = this.createAddMarkerButtonElem();
            this.overlay = new google.maps.OverlayView();
            this.overlay.draw = function () {
                this.getPanes().markerLayer.id = 'markerLayer';
            };
        }

        createAddMarkerButtonElem() {
            let addMarkerDiv = document.createElement('div');
            addMarkerDiv.style.margin = '0 60px 24px 60px';
            addMarkerDiv.index = 1;
            let controlButton = document.createElement('div');
            controlButton.className = 'control-button';
            controlButton.innerText = R1;

            controlButton.addEventListener('click', () => this.togglePlaceMarker());
            addMarkerDiv.appendChild(controlButton);
            return {addMarkerDiv: addMarkerDiv, controlButton: controlButton};
        }

        // Размещает маркер в точку
        placeNewPlaceMarker(location) {
            if (this.newPlaceMarker) {
                this.newPlaceMarker.setMap(null);
            }
            this.newPlaceMarker = new google.maps.Marker({
                position: location,
                cursor: 'pointer',
                map: this.map
            });
        }

        // Добавляет EventPlace'ы на карту
        setMarkers(places) {
            for (const place of places) {
                let eventPlaceMarker = new google.maps.Marker({
                    // shape определяет кликабельную зону маркера. type - атрибут <area>
                    // Элемент <area> типа 'poly' задаётся как ряд чередующихся координат точек X, Y.
                    // Конечная координата замыкает полигон, присоеденяясь к первой координате.
                    shape: {
                        type: 'poly',
                        coords: [1, 1, 1, 20, 18, 20, 18, 1],
                    },
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: 8.5,
                        fillColor: getCssVar('yellow'),
                        fillOpacity: 1,
                        strokeWeight: 1,
                        strokeColor: 'white',
                    },
                    position: new google.maps.LatLng(Number(place.latitude), Number(place.longitude)),
                    map: this.map,
                    zIndex: place.id,
                    title: place.title,
                    optimized: false,
                });

                eventPlaceMarker.addListener('click', () => {
                    // noinspection JSIgnoredPromiseFromCall
                    NefarApp.changeStage({
                        stage: 3, eventTypeIds: this.eventTypeIds, boundsStr: this.bounds.toJSON(),
                        placeId: place.id, markerIndex: eventPlaceMarker.zIndex
                    });
                });

                this.indexMarkerMap.set(eventPlaceMarker.zIndex, eventPlaceMarker);
            }
        }

        // Переключает возможность размещать, предлогаемое модератору, место PotentialEventPlace
        togglePlaceMarker() {
            this.canPlaceMarker = !this.canPlaceMarker;
            this.boundsRectangle.setOptions({clickable: this.canPlaceMarker});
            if (this.canPlaceMarker) {
                this.addMarkerButton.controlButton.classList.add('control-button_clicked');
            } else {
                this.addMarkerButton.controlButton.classList.remove('control-button_clicked');
            }
        }

        async show(bounds, eventTypeIds) {
            this.eventTypeIds = eventTypeIds;
            this.bounds = bounds;

            if (!this.map) {
                this.map = new google.maps.Map(document.querySelector('#map-window__map'), {mapTypeId: google.maps.MapTypeId.ROADMAP});
                this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(this.addMarkerButton.addMarkerDiv);
                this.overlay.setMap(this.map);
            }

            this.boundsRectangle = new google.maps.Rectangle({
                bounds: {
                    north: this.bounds.getNorthEast().lat(),
                    south: this.bounds.getSouthWest().lat(),
                    east: this.bounds.getNorthEast().lng(),
                    west: this.bounds.getSouthWest().lng()
                },
                clickable: false,
                fillColor: '',
                fillOpacity: 0,
                map: this.map,
                strokeColor: 'red',
                strokeOpacity: 0.8,
                strokeWeight: 2
            });

            // Добавление EventPlace
            google.maps.event.addListener(this.boundsRectangle, 'click', event => {
                if (this.canPlaceMarker) {
                    let latitude = event.latLng.lat();
                    let longitude = event.latLng.lng();
                    this.placeNewPlaceMarker(event.latLng);

                    this.geocoder.geocode({'location': {lat: latitude, lng: longitude}}, (results, status) => {
                        new Modal(
                            'add-place',
                            function () {
                                this.pageElem.querySelector('#modal__latitude').value = latitude.toFixed(5);
                                this.pageElem.querySelector('#modal__longitude').value = longitude.toFixed(5);
                                this.pageElem.querySelector('#modal__place-address').innerText =
                                    (status === google.maps.GeocoderStatus.OK) ? results[0]['formatted_address'] : '';
                            },
                            function () {
                                ajaxPost('add_event_place', {}, new FormData(this.pageElem.querySelector('form')))
                                    .then(() => {
                                        NefarApp.MapWindow.togglePlaceMarker();
                                        NefarApp.MapWindow.newPlaceMarker.setMap(null);
                                        new Modal('be-consider').show();
                                    });
                            },
                            function () {
                                NefarApp.MapWindow.newPlaceMarker.setMap(null);
                            }
                        ).show();
                    });
                }
            });

            this.places = await ajaxGetJSON('get_place_positions',
                {
                    lat1: this.bounds.getNorthEast().lat(),
                    lat2: this.bounds.getSouthWest().lat(),
                    lng1: this.bounds.getNorthEast().lng(),
                    lng2: this.bounds.getSouthWest().lng(),
                    type_ids: eventTypeIds
                }
            );
            this.setMarkers(this.places);

            this.pageElem.style.display = '';
            // Должно быть видимо
            this.map.fitBounds(this.bounds);
        }

        removeMarkers() {
            for (let [_, marker] of this.indexMarkerMap) {
                marker.setMap(null);
            }
            this.indexMarkerMap.clear();
        }
    };

    this.EventTypesWindow = new class extends Window {
        constructor() {
            super('event-types-window');

            this.sliderElem = this.pageElem.querySelector('#event-types-window__slider');
            this.slideElem = this.sliderElem.querySelector('#event-types-window__slide');
            this.dotsElem = this.sliderElem.querySelector('#event-types-window__dots');
            this.cellsElem = this.slideElem.querySelector('#event-types-window__cells');
            this.cellSize = 240;
            this.dotSize = 15;
            this.dotMargin = 2;
            this.dotsElemBottomPadding = parseInt(getCssVar('content-padding'));
            this.heightLimitParent = this.pageElem.parentElement;
            this.pageIndex = 1;
            this.prevElem = this.slideElem.querySelector('#event-types-window__prev');
            this.prevElem.addEventListener('click', () => this.showPrevPage());
            this.nextElem = this.slideElem.querySelector('#event-types-window__next');
            this.nextElem.addEventListener('click', () => this.showNextPage());
            this.resizeHandler = this.redraw.bind(this);
        }

        addCell(cell) {
            this.cells.push(cell);
        }

        redraw() {
            this.dotsElem.style.display = '';
            this.prevElem.style.display = '';
            this.nextElem.style.display = '';
            this.cellsElem.querySelectorAll('.event-type-cell').forEach(cell => cell.remove());
            this.slideElem.style.flexGrow = '';

            let cellsContainerWidth = parseFloat(window.getComputedStyle(this.cellsElem).width);
            let dotsContainerWidth = parseFloat(window.getComputedStyle(this.dotsElem).width);
            let dotsElemHeight = this.dotSize + this.dotMargin * 2 + this.dotsElemBottomPadding;

            this.linesCount = 0;
            this.dotLines = 0;
            let cellsPerLine = Math.floor(cellsContainerWidth / this.cellSize);
            let dotsPerLine = Math.floor(dotsContainerWidth / dotsElemHeight);
            do {
                this.linesCount++;
                this.cellsPerPage = this.linesCount * cellsPerLine;
                this.pagesCount = Math.ceil(this.cells.length / this.cellsPerPage);
                if (this.pagesCount === 1) {
                    this.dotLines = 0;
                } else {
                    this.dotLines = Math.ceil(this.pagesCount / dotsPerLine);
                }
            } while (this.dotLines * dotsElemHeight + this.linesCount * this.cellSize <= this.heightLimitParent.clientHeight);

            this.linesCount--;
            this.cellsPerPage = this.linesCount * cellsPerLine;
            this.pagesCount = Math.ceil(this.cells.length / this.cellsPerPage);
            if (this.pagesCount === 1) {
                this.dotLines = 0;
            } else {
                this.dotLines = Math.ceil(this.pagesCount / dotsPerLine);
            }

            if (this.linesCount === 0) {
                this.cellsPerPage = cellsPerLine;
                this.pagesCount = Math.ceil(this.cells.length / this.cellsPerPage);
            }

            for (let i = this.cellsPerPage * (this.pageIndex - 1);
                 i < Math.min(this.cellsPerPage * this.pageIndex, this.cells.length); ++i) {
                this.cellsElem.appendChild(this.cells[i].pageElem);
            }
            let cells = this.cellsElem.querySelectorAll('.event-type-cell');
            for (let cell of cells) {
                cell.style.width = this.cellSize + 'px';
            }
            if (this.linesCount === 0) {
                this.slideElem.style.flexGrow = '1';
            } else {
                for (let cell of cells) {
                    cell.style.height = this.cellSize + 'px';
                }
            }
            for (let cell of cells) {
                cell.querySelector('.event-type-cell__icon').style.fontSize =
                    cell.querySelector('.event-type-cell__hover-bg').clientHeight + 'px';
            }

            this.dotsElem.querySelectorAll('.dot').forEach(dot => dot.remove());
            if (this.pagesCount === 1) {
                this.dotsElem.style.display = 'none';
                this.prevElem.style.display = 'none';
                this.nextElem.style.display = 'none';
            } else {
                for (let i = 1; i <= this.pagesCount; ++i) {
                    let dotElem = document.createElement('span');
                    dotElem.className = 'dot';
                    dotElem.style.width = this.dotSize + 'px';
                    dotElem.style.height = this.dotSize + 'px';
                    dotElem.style.margin = this.dotMargin + 'px';
                    dotElem.addEventListener('click', () => this.showPage(i));
                    this.dotsElem.appendChild(dotElem);
                }
                document.querySelector(`.dot:nth-child(${this.pageIndex})`).classList.add('selected');
            }
        }

        showNextPage() {
            this.pageIndex += 1;
            if (this.pageIndex > this.pagesCount) {
                this.pageIndex = 1;
            }
            this.redraw();
        }

        showPrevPage() {
            this.pageIndex -= 1;
            if (this.pageIndex < 1) {
                this.pageIndex = this.pagesCount;
            }
            this.redraw();
        }

        showPage(n) {
            this.pageIndex = n;
            this.redraw();
        }

        async show(bounds) {
            this.cells = [];
            window.addEventListener('resize', this.resizeHandler);

            const eventTypes = await ajaxGetJSON('get_event_types',
                {
                    lat1: bounds.getNorthEast().lat(),
                    lat2: bounds.getSouthWest().lat(),
                    lng1: bounds.getNorthEast().lng(),
                    lng2: bounds.getSouthWest().lng()
                }
            );

            if (!eventTypes.length) {
                this.hide();
                new Modal('no-events').show();
                // noinspection ES6MissingAwait
                NefarApp.changeStage({stage: 0});
                return;
            }

            this.addCell(new EventTypeCell('fal fa-globe', R3, () => {
                this.hide();
                NefarApp.changeStage({
                    stage: 2,
                    boundsStr: bounds.toJSON(),
                    eventTypeIds: eventTypes.map(type => type.id)
                });
            }));

            for (let type of eventTypes) {
                this.addCell(new EventTypeCell(type.icon, type.name, () => {
                    this.hide();

                    // TODO множественный выбор ячеек
                    NefarApp.changeStage({
                        stage: 2,
                        boundsStr: bounds.toJSON(),
                        eventTypeIds: Array.of(type.id)
                    });
                }));
            }

            this.pageElem.style.display = '';
            this.redraw();
        }

        hide() {
            super.hide();
            window.removeEventListener('resize', this.resizeHandler);
        }
    };

    this.SearchWindow = new class extends Window {
        constructor() {
            super('search-window');
            this.input = document.querySelector('#search-window__search-box');

            // [Фикс выпадающий список ниже bodyElem
            document.addEventListener('focus', async () => {
                if (this.input.value) {
                    if (document.activeElement === this.input) {
                        await this.moveInputToTop();
                    } else {
                        this.moveInputToCenter();
                    }
                }
            }, true); // Без true не работает focus

            this.input.addEventListener('input', async () => {
                if (this.input.value && this.pageElem.style.alignItems === 'center') {
                    await this.moveInputToTop();
                } else if (!this.input.value && this.pageElem.style.alignItems === 'baseline') {
                    this.moveInputToCenter();
                }

            });
            // Фикс выпадающий список ниже bodyElem]


            this.autocomplete = new google.maps.places.Autocomplete(this.input, {types: ['(cities)']});

            // [Выбирает первый в списке при нажатии Enter
            (function enableEnterKey(input) {
                const _addEventListener = input.addEventListener;

                input.addEventListener = (type, listener) => {
                    if (type === 'keydown') {
                        const _listener = listener;
                        listener = (event) => {
                            if (event.which === 13 && !document.getElementsByClassName('pac-item-selected').length) { // noinspection JSCheckFunctionSignatures
                                _listener.apply(input, [new KeyboardEvent('keypress', {keyCode: 40})]);
                            }

                            _listener.apply(input, [event])
                        }
                    }
                    _addEventListener.apply(input, [type, listener])
                };
            })(this.input);
            // Выбирает первый в списке при нажатии Enter]

            // Событие вызывается когда место выбрано
            this.autocomplete.addListener('place_changed', () => {
                let foundPlace = this.autocomplete.getPlace();

                if (!foundPlace.geometry) {
                    console.log('Returned foundPlace contains no geometry');
                    return;
                }

                // Находим границы города
                let bounds = new google.maps.LatLngBounds();
                if (foundPlace.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(foundPlace.geometry.viewport);
                } else {
                    bounds.extend(foundPlace.geometry.location);
                }

                // Скрыть окно поиска и вернуть результат
                this.hide();

                // noinspection JSIgnoredPromiseFromCall
                NefarApp.changeStage({stage: 1, boundsStr: bounds.toJSON()});
            });
        }

        moveInputToCenter() {
            this.pageElem.style.alignItems = 'center';
        }

        async moveInputToTop() {
            let pac = (await condReturn(
                () => document.getElementsByClassName('pac-container pac-logo'),
                () => {
                    let pacs = document.getElementsByClassName('pac-container pac-logo');
                    return pacs.length && pacs[0].style.display !== 'none';
                }
            ))[0];

            const pacShadowHeight = parseInt(getComputedStyle(pac).boxShadow.split(/\s(?![^(]*\))/)[3]);
            if (getElemBottom(pac) + pacShadowHeight > getElemBottom(this.pageElem) && this.pageElem.style.alignItems === 'center') {
                this.pageElem.style.alignItems = 'baseline';
                this.updPacPos(pac);
                if (getElemBottom(pac) + pacShadowHeight > getElemBottom(this.pageElem)) {
                    this.pageElem.style.height = this.pageElem.offsetHeight + getElemBottom(pac) - getElemBottom(this.pageElem) + pacShadowHeight + 'px';
                }
            }
        }

        // Разрыл то как перерисовывается в скрипте Google, требует слежения за соответствием
        updPacPos(pac) {
            pac.style.top = (this.input.getBoundingClientRect().top + (this.input.style.borderTopWidth ? parseInt(this.input.style.borderTopWidth) : 0))
                + (window && window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop || 0)
                + this.input.offsetHeight - (this.input.style.borderTopWidth ? parseInt(this.input.style.borderTopWidth) : 0) + 'px';
        }

        async show() {
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }

            // noinspection JSIgnoredPromiseFromCall
            super.show();
            await sleep(2000);
            this.input.focus();

            this.input.value = 'Kyiv';
            await sleep(3000);
            NefarApp.changeStage({
                stage: 1,
                boundsStr: {south: 50.213273, west: 30.239440100000024, north: 50.590798, east: 30.825941000000057}
            });
        }
    };

    this.pickKeys = function(obj, keys) {
        return keys.reduce((function (obj, property) {
            obj[property] = this[property];
            return obj;
        }).bind(obj), {});
    };

    // noinspection JSCheckFunctionSignatures
    const EventPlaceMobileCard = EventPlace(Mobile(Card));
    // noinspection JSCheckFunctionSignatures
    const EventPlaceDesktopCard = EventPlace(Desktop(Card));
    // noinspection JSCheckFunctionSignatures
    const UserBarsMobileCard = UserBars(Mobile(Card));
    // noinspection JSCheckFunctionSignatures
    const UserBarsDesktopCard = UserBars(Desktop(Card));

    this.renewedWindow = (newWindow) => {
        if (this.currentWindow) {
            if (this.currentWindow !== newWindow) {
                this.currentWindow.hide();
                this.currentWindow = newWindow;
                return this.currentWindow;
            }
        } else {
            this.currentWindow = newWindow;
            return this.currentWindow;
        }
    };

    this.changeStage = async function (params, pushHistory = true) {
        this.lastParams = this.params;
        this.params = this.pickKeys(params, {
            0: ['stage'],
            1: ['stage', 'boundsStr'],
            2: ['stage', 'boundsStr', 'eventTypeIds'],
            3: ['stage', 'boundsStr', 'eventTypeIds', 'placeId', 'markerIndex'],
            4: ['stage', 'boundsStr', 'eventTypeIds', 'placeId', 'markerIndex', 'placeName', 'eventId'],
        }[params.stage]);

        if (pushHistory) {
            window.history.pushState(null, null, dictToUrlParams(this.params));
        }

        if (this.lastParams && this.lastParams.stage >= 2 && this.params.stage < 2) {
            this.MapWindow.removeMarkers();
        }
        if (this.currentCard) {
            this.currentCard.close();
        }

        let window_ = this.renewedWindow.call(this, {
            0: this.SearchWindow,
            1: this.EventTypesWindow,
            2: this.MapWindow,
            3: this.MapWindow,
            4: this.MapWindow,
        }[this.params.stage]);

        function get1(x, o) {

        }
        get1(this.params.stage, {
            1: 2,
        });

        if (window_) {
            await window_.show.apply(window_, {
                get 0() { return [] },
                get 1() { return [strToBounds(NefarApp.params.boundsStr)] },
                get 2() { return [strToBounds(NefarApp.params.boundsStr), NefarApp.params.eventTypeIds] },
                get 3() { return [strToBounds(NefarApp.params.boundsStr), NefarApp.params.eventTypeIds] },
                get 4() { return [strToBounds(NefarApp.params.boundsStr), NefarApp.params.eventTypeIds] },
            }[this.params.stage]);
        }

        switch (this.params.stage) {
            case 3:
                let placeDetails = await ajaxGetJSON('get_place_details', {
                    place_id: this.params.placeId,
                    type_ids: this.params.eventTypeIds
                });
                if (this.MapWindow.indexMarkerMap.get(this.params.markerIndex)) {
                    this.showCard(EventPlaceMobileCard, EventPlaceDesktopCard, placeDetails);
                } else {
                    this.changeStage({
                        stage: 2,
                        boundsStr: this.params.boundsStr,
                        eventTypeIds: this.params.eventTypeIds
                    });
                }
                break;
            case 4:
                let eventDetails = await ajaxGetJSON('get_event_details', {'event_id': this.params.eventId});
                if (this.MapWindow.indexMarkerMap.get(this.params.markerIndex)) {
                    this.showCard(UserBarsMobileCard, UserBarsDesktopCard, eventDetails);
                } else {
                    this.changeStage({
                        stage: 2,
                        boundsStr: this.params.boundsStr,
                        eventTypeIds: this.params.eventTypeIds
                    });
                }
                break;
        }
    };

    this.showCard = function (mobileCard, desktopCard, ...args) {
        const renewCard = newCard => {
            if (this.currentCard &&
                ((this.currentCard.constructor !== newCard || this.params !== this.lastParams) ||
                    (this.currentCard.constructor === desktopCard && this.showCard.caller === window.onresize))) {
                this.currentCard.close();
                this.currentCard = new newCard(...args);
            } else if (!this.currentCard) {
                this.currentCard = new newCard(...args);
            }
        };

        if (isMobile()) {
            renewCard(mobileCard);
        } else {
            renewCard(desktopCard);
        }

        window.onresize = () => {
            if (this.currentCard.isShown) {
                this.showCard(mobileCard, desktopCard, ...args);
            }
        };
    }
};

// noinspection JSIgnoredPromiseFromCall
NefarApp.changeStage(getUrlParams());
window.onbeforeunload = () => window.scrollTo(0, 0);
window.onpopstate = () =>
    NefarApp.changeStage(getUrlParams(), false);