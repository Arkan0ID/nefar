from django.contrib.auth.views import LoginView, LogoutView
from django.urls import re_path, reverse_lazy
from django.views.generic import CreateView

from activities import views
from activities.forms import SignupForm, LoginForm

urlpatterns = [
    re_path(r'^$', views.index),
    re_path(r'^login/$', LoginView.as_view(template_name='activities/login.html', redirect_authenticated_user=True,
                                           authentication_form=LoginForm), name='login'),
    re_path(r'^logout/$', LogoutView.as_view(), name='logout'),
    re_path(r'^signup/$', CreateView.as_view(template_name='activities/signup.html', form_class=SignupForm,
                                             success_url=reverse_lazy(views.index)), name='signup'),
    re_path(r'^get_event_types/', views.get_event_types),
    re_path(r'^get_place_positions/', views.get_place_positions),
    re_path(r'^add_event_place/$', views.add_event_place),
    re_path(r'^get_place_details/', views.get_place_details),
    re_path(r'^get_event_details/', views.get_event_details),
    re_path(r'^add_event/', views.add_event),
]
