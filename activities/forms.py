from django import forms
from django.contrib.auth.forms import UsernameField, UserCreationForm, AuthenticationForm
from django.forms import ModelForm
from activities.models import ActualEventPlace, User
from django.utils.translation import gettext as _
from django.utils.text import capfirst


class SignupForm(UserCreationForm):
    # max_length - ru.wikipedia.org/wiki/E.164#Категории
    phone_number = forms.CharField(max_length=15, label=_('Phone number'))
    telegram_username = forms.CharField(max_length=32, label=_('Telegram username'))

    class Meta:
        model = User
        fields = ('username', 'email', 'phone_number', 'telegram_username',)
        field_classes = {'username': UsernameField, 'email': forms.EmailField, 'phone_number': forms.CharField,
                         'telegram_username': forms.CharField}


class AddEventPlaceForm(ModelForm):
    class Meta:
        model = ActualEventPlace
        fields = ('description', 'name', 'latitude', 'longitude')


class LoginForm(AuthenticationForm):
    username = UsernameField(label='',
                             widget=forms.TextInput(
                                 attrs={'autofocus': True, 'placeholder': _(capfirst('username')), 'value': 'test'}))
    password = forms.CharField(label='', strip=False,
                               widget=forms.PasswordInput(attrs={'placeholder': _('Password'), 'value': 'test'}))
