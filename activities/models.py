from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.db.models import CASCADE
from django.utils.translation import gettext as _


class User(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(_('email'), blank=True)
    # max_length - ru.wikipedia.org/wiki/E.164#Категории
    phone_number = models.CharField(max_length=15)
    telegram_username = models.CharField(max_length=32, null=True)

    # Обязатеьно для createsuperuser
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __repr__(self):
        return self.username


# Место где могуть проходить события
class EventPlace(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=150, blank=True)
    description = models.TextField(max_length=140)
    latitude = models.DecimalField(max_digits=9, decimal_places=7, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=7, null=True)
    image = models.ImageField(default='media/default.png')

    def get_marker_data(self):
        return {'title': self.name, 'position': {
                                        'lat': float(self.latitude),
                                        'lng': float(self.longitude)
                                    }, 'id': self.id}

    def __repr__(self):
        return f'{self.name}\n{self.description}\nLat: {self.latitude}\nLng: {self.longitude}'


class ActualEventPlace(EventPlace):
    pass


class PotentialEventPlace(EventPlace):
    def confirm(self):
        ActualEventPlace.objects.create(**{field.name: getattr(self, field.name) for field in EventPlace._meta.fields})
        self.delete()

    def reject(self):
        self.delete()


# Вид события
class EventType(models.Model):
    name = models.CharField(max_length=150)
    icon = models.CharField(max_length=150)

    def __repr__(self):
        return self.name


# Событие
class Event(models.Model):
    place = models.ForeignKey(ActualEventPlace, on_delete=CASCADE, related_name='events')
    name = models.CharField(max_length=255)
    time = models.DateTimeField('Date of the event')
    type = models.ForeignKey(EventType, on_delete=CASCADE)
    organizer = models.ForeignKey(User, on_delete=CASCADE)
    users = models.ManyToManyField(User, related_name='events')
