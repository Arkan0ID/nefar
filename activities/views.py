import json
import operator
from functools import reduce

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_POST, require_GET

from activities.forms import AddEventPlaceForm
from activities.models import EventType, PotentialEventPlace, Event, ActualEventPlace


@login_required
@require_GET
def index(request):
    return render(request, 'activities/index.html')


@login_required
@require_POST
def add_event_place(request):
    form = AddEventPlaceForm(request.POST)
    print(form)
    if not form.is_valid():
        return JsonResponse({'errors': form.errors})
    data = form.cleaned_data
    event_place = PotentialEventPlace.objects.create(**data)
    print(data)
    event_place.save()
    return HttpResponse()


@login_required
@require_GET
def get_place_positions(request):
    latitude_range = sorted([float(request.GET.get('lat1')), float(request.GET.get('lat2'))])
    longitude_range = sorted([float(request.GET.get('lng1')), float(request.GET.get('lng2'))])

    event_places = ActualEventPlace.objects.filter(
        Q(
            latitude__gte=latitude_range[0], latitude__lte=latitude_range[1],
            longitude__gte=longitude_range[0], longitude__lte=longitude_range[1],
        ),
        reduce(operator.or_, (Q(events__type__id__contains=type_) for type_ in json.loads(request.GET.get('type_ids'))))
    ).distinct()

    return JsonResponse(list(event_places.values('id', 'latitude', 'longitude')), safe=False)


@login_required
@require_GET
def get_event_types(request):
    latitude_range = sorted([float(request.GET.get('lat1')), float(request.GET.get('lat2'))])
    longitude_range = sorted([float(request.GET.get('lng1')), float(request.GET.get('lng2'))])

    # .distinct() - уникальные
    event_types = list(EventType.objects.filter(event__place__latitude__gte=latitude_range[0],
                                                event__place__latitude__lte=latitude_range[1],
                                                event__place__longitude__gte=longitude_range[0],
                                                event__place__longitude__lte=longitude_range[1])
                       .values().distinct())
    if event_types:
        return JsonResponse(event_types, safe=False)
    else:
        return JsonResponse([], safe=False)


@login_required
@require_GET
def get_place_details(request):
    place_details = \
        ActualEventPlace.objects.filter(id=request.GET.get('place_id')).values('name', 'description', 'image')[0]
    place_details['events'] = list(Event.objects.filter(place_id=request.GET.get('place_id'),
                                                        type_id__in=json.loads(request.GET.get('type_ids'))).values(
        'id', 'name', 'time', 'type__icon'))
    for field in place_details['events']:
        field['time'] = field['time'].strftime('%d.%m.%y %H:%M')
    return JsonResponse(place_details)


@login_required
@require_GET
def get_event_details(request):
    event = Event.objects.get(id=request.GET.get('event_id'))
    return JsonResponse({'organizer': event.organizer.telegram_username,
                         'users': list(event.users.values_list('telegram_username', flat=True))})


def add_event(request):
    return None