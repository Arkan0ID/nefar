from django.contrib import auth
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect


User = auth.get_user_model()


class UserAutoLogin:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        if not settings.DEBUG:
            return

        auth_user = auth.authenticate(username='test', password='test')

        if not auth_user:
            if getattr(settings, 'LOGIN_URL'):
                return redirect(getattr(settings, 'LOGIN_URL'))

            raise PermissionDenied

        auth.login(request, auth_user)

        response = self.get_response(request)
        # Code to be executed for each request/response after
        # the view is called.

        return response
