from os import getcwd
from os import path
from extensions import File, run_py, run_cmd, install_package, chdir, init_django, INIT_DIR, PROJ_DIR
settings = init_django()

from allauth.socialaccount.models import SocialApp
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site

base_dir = getcwd()

while True:
    s = input('Enter whether it server or local: ')
    if s in ['server', 'local']:
        break

# Обновляем библиотеки
install_package('-U', 'Django==2.1.10', 'Pillow')

with chdir(PROJ_DIR):
    run_cmd('git submodule update --init --recursive --remote')

    if s == 'server':
        # Собираем статические файлы
        run_py('manage.py collectstatic --noinput')

    db_file = File(settings.DATABASES['default']['NAME'])
    if db_file.exists:
        if input('Delete db? (y):\n') == 'y':
            while True:
                try:
                    db_file.remove()
                    break
                except Exception as e:
                    input(e)
            run_py('manage.py migrate')

            site = Site.objects.first()
            site.name = site.domain = 'localhost:8000'
            site.save()

            get_user_model().objects.create_superuser('Arkan0ID', 'vinnik.dmitry07@gmail.com',
                                                      input('Admin password:\n'))

            print('https://console.developers.google.com/apis/credentials/')
            app = SocialApp.objects.create(name='Google API', provider='google', client_id=input('client_id:\n'),
                                           secret=input('secret:\n'))
            app.sites.add(site)
            app.save()
    else:
        run_py('manage.py migrate')


with chdir(INIT_DIR):
    if s == 'server':
        # Копируем скрипт подтягивания проекта в базовую директорию облака
        with open('pull.sh', 'r') as in_:
            with open(path.join(base_dir, 'pull'), 'w') as out:
                x = in_.read().format(PROJ_DIR)
                out.write(x)

    run_py('init_gitignore.py')  # Обновляем .gitignore
    run_py('init_models.py')
