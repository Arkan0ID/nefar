from extensions import single_checkout, Files, File

single_checkout('https://github.com/github/gitignore.git', 'Global/JetBrains.gitignore', 'JetBrains.gitignore')
Files('base.gitignore', 'JetBrains.gitignore').join('../.gitignore')
File('JetBrains.gitignore').remove()
