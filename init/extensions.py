import os
import shutil
import stat
import subprocess
import sys
from os.path import dirname, abspath

# noinspection PyProtectedMember
from pip._internal import main as pip_main

INIT_DIR = abspath(dirname(__file__))
PROJ_DIR = dirname(INIT_DIR)


class chdir:
    def __init__(self, path):
        self.path = path
        os.chdir(self.path)

    def __enter__(self):
        print('Current dir', self.path)

    def __exit__(self, exc_type, exc_value, traceback):
        pass


def run_py(command):
    run_cmd(sys.executable + ' ' + command)


def run_cmd(command):
    print('Running: ' + command)
    subprocess.run(command, shell=True, check=True)


def install_package(*args):
    print('Installing: ' + ' '.join(args))
    pip_main(['install'] + list(args))


class File:
    def __init__(self, path):
        self.path = path

    def copy(self, dst):
        shutil.copyfile(self.path, dst)
        print('Copied:', self.path)

    def remove(self):
        os.remove(self.path)
        print('Removed:', self.path)

    @property
    def exists(self):
        return os.path.isfile(self.path)


class Files:
    def __init__(self, *args: str):
        self.file_names = args

    def join(self, output_path):
        with open(output_path, 'w') as outfile:
            for name in self.file_names:
                with open(name) as infile:
                    for line in infile:
                        outfile.write(line)


class Dir:
    # Fix: some .git directory files cannot be deleted due to readonly attribute
    @staticmethod
    def __ignore_errors(func, path, exc_info):
        if type(exc_info[1]) == PermissionError and '.git' in exc_info[1].filename:
            if not os.access(path, os.W_OK):
                os.chmod(path, stat.S_IWUSR)
                func(path)
        else:
            raise exc_info[1]

    def __init__(self, path):
        self.path = path

    def remove(self):
        shutil.rmtree(self.path, onerror=self.__ignore_errors)


def single_checkout(url, remote_path, local_path):
    subprocess.call(f'git clone -n "{url}" --depth 1', shell=True)
    temp = url.split('/')[-1]
    if not temp.endswith('.git'):
        raise Exception('Given url is not git-like!')
    dir_name = temp[:-4]
    subprocess.call(f'git checkout HEAD "{remote_path}"', shell=True, cwd=dir_name)
    shutil.move(os.path.join(dir_name, remote_path), local_path)
    Dir(dir_name).remove()


def get_proj_dir():
    return abspath(dirname(dirname(__file__)))


def init_django():
    import config
    sys.path.append(PROJ_DIR)
    import django
    from django.conf import settings
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', config.PACKAGE_NAME + '.settings')
    django.setup()
    return settings
