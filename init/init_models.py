from extensions import init_django

init_django()

from activities.models import ActualEventPlace, EventType, Event, User
from django.utils import timezone
from decimal import Decimal

ActualEventPlace.objects.all().delete()
EventType.objects.all().delete()
Event.objects.all().delete()
User.objects.filter(is_superuser=False).delete()

for aep in [
    {
        'name': 'Basketball playground on KNU campus',
        'description': 'Two good baskets, the backboard is not well, the cover is bearable, '
                       'there is marking.',
        'latitude': Decimal('50.387729'), 'longitude': Decimal('30.477908'),
        'image': 'media/playground.png',
    },
    {
        'name': 'Football field on KNU campus',
        'description': 'There is a football goal, the cover is rubber',
        'latitude': Decimal('50.387349'), 'longitude': Decimal('30.475076')
    },
    {
        'name': 'Andersen Brand Pub',
        'description': 'The first brand pub of the new Kyiv craft beer maker ANDERSEN. The ANDERSEN Brand Pub offers '
                       'its gastronomic journey to Scandinavia\'s many islands, washed by waves of freshly brewed foam '
                       'and saturated with the spirit of mysterious Scandinavian-Baltic cuisine. Northern gastronomy '
                       'wins the hearts of even the most discerning gourmets with its amazing variety of fish, seafood '
                       'and game dishes, non-trivial combinations of ingredients, simplicity and sophistication of '
                       'taste impressions. Easy, complete and unique cuisine at ANDERSEN Brand Pub - everyone can '
                       'discover completely new tastes and impressions here!',
        'latitude': Decimal('50.3867383'), 'longitude': Decimal('30.4626257'),
        'image': 'media/andersen.png',
    },
    {
        'name': 'BINGO club',
        'description': 'One of the main cultural and entertainment complexes in Kyiv. The club has been entertaining '
                       'its guests with concerts of the best domestic and foreign artists for over 15 years. To date, '
                       'Bingo is one of the largest concert clubs in Ukraine, with a capacity of 1500 spectators, with '
                       'its technical facilities, a large billiard room, the best place for banquets and banquets, '
                       'a strip bar and a lounge cafe. The club also has free wifi internet access.',
        'latitude': Decimal('50.4572367'), 'longitude': Decimal('30.381108'),
        'image': 'media/bingo.png',
    },
]:
    ActualEventPlace.objects.create(**aep).save()

# noinspection SpellCheckingInspection
for et in [{'name': 'Basketball', 'icon': 'fal fa-basketball-ball'}, {'name': 'Football', 'icon': 'fal fa-futbol'},
           {'name': 'Have a beer', 'icon': 'fal fa-beer'}, {'name': 'Music', 'icon': 'fal fa-music'}]:
    EventType.objects.create(**et)

user_test = User.objects.create_user(username='test', email='test@test.com', password='test',
                                     telegram_username='vinnik_dmitry07')
user_trof = User.objects.create_user(username='trof', email='test@test.com', password='trof',
                                     telegram_username='trofimov00')

e1 = Event(place=ActualEventPlace.objects.all()[0], name='Evening play', time=timezone.now(),
           type=EventType.objects.all()[0], organizer=user_test)
e1.save()
e1.users.set([user_trof])

e2 = Event(place=ActualEventPlace.objects.all()[1], name='A game', time=timezone.now(),
           type=EventType.objects.all()[1], organizer=user_test)
e2.save()
e2.users.set([user_trof])

e3 = Event(place=ActualEventPlace.objects.all()[2], name='Beer contest', time=timezone.now(),
           type=EventType.objects.all()[2], organizer=user_test)
e3.save()
e3.users.set([user_trof])

e4 = Event(place=ActualEventPlace.objects.all()[3], name='Rockstage', time=timezone.now(),
           type=EventType.objects.all()[3], organizer=user_test)
e4.save()
e4.users.set([user_trof])
