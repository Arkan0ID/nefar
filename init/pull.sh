#!/usr/bin/env bash
# Подтягивает обновления с хостинга
# . pull(.sh) === source pull(.sh)
workon venv &&
cd {} &&
git config --global user.email 'vinnik.dmitry07@gmail.com' &&
git config --global user.name 'Dmitry' &&
git stash &&
git pull origin master